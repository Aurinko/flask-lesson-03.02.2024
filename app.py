from flask import Flask, render_template, request

from processing import calculate_cost

app = Flask(__name__)


@app.route('/', methods=["get", "post"])
def calculate():
    message = ""
    if request.method == "POST":
        area = request.form.get("area")
        try:
            area = float(area)
        except:
            message = "Введены некорректные значения, попробуйте снова"
            return render_template("index.html", message=message)

        cost = calculate_cost(area)
        message = f"Стоимость квартиры площадью {area} кв. м. равна {cost} рублей"

    return render_template("index.html", message=message)


# app.run()
