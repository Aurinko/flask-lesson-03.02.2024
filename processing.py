import pickle

import numpy as np


def load_model(path="rf.pkl"):
    with open(path, "rb") as f:
        model = pickle.load(f)
    return model


def load_scaler(path):
    with open(path, "rb") as f:
        scaler = pickle.load(f)
    return scaler


def calculate_cost(area):
    model = load_model(path="models/rf.pkl")
    scaler_x = load_scaler(path="models/scaler_x.pkl")
    scaler_y = load_scaler(path="models/scaler_y.pkl")

    params = scaler_x.transform([[area]])
    y_pred = model.predict(params)

    cost = scaler_y.inverse_transform([y_pred])[0][0]
    cost = np.round(cost, 2)

    return cost
